import re

from django.conf import settings
from django.contrib.auth import (authenticate, login as login_user,
                                 logout as logout_user)
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.urls import reverse

from accounts.exceptions import CannotConfirmEmail
from accounts.models import User


def login(request):
    if request.user.is_authenticated:
        return redirect('index')

    username = None
    errors = {}

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        def check_credentials():
            if not username:
                errors['username'] = 'You forgot to provide your username!'
            if not password:
                errors['password'] = 'You forgot to enter your password!'

            if errors:
                return

            user = authenticate(request, username=username, password=password)
            if user is None:
                errors['password'] = "That ain't the right password, bub."

            return user

        user = check_credentials()
        if user is not None:
            login_user(request, user)
            return redirect('index')

    return render(request, 'login.html', {
        'social_auth_facebook_key': settings.SOCIAL_AUTH_FACEBOOK_KEY,
        'username': username,
        'errors': errors
    })


def logout(request):
    logout_user(request)
    return redirect('index')


def register(request):
    if request.user.is_authenticated:
        return redirect('index')

    username = None
    real_name = None
    email = None
    errors = {}

    if request.method == 'POST':
        username = request.POST.get('username')
        real_name = request.POST.get('real_name', '')
        email = request.POST.get('email')
        password = request.POST.get('password')
        password_confirmation = request.POST.get('password_confirmation')

        # TODO: Django probably has a built-in form I should be using for this
        def check_credentials():
            if not username:
                errors['username'] = 'You forgot to provide your username!'
            if not email:
                errors['email'] = 'You forgot to provide an e-mail address!'
            if not password:
                errors['password'] = 'You forgot to enter your password!'
            if password != password_confirmation:
                errors['password_confirmation'] = "The two passwords you entered didn't match. Type carefully!"

            if email:
                try:
                    validate_email(email)
                except ValidationError:
                    errors['email'] = 'That is not a valid e-mail address.'

            if len(errors) > 0:
                return False

            if User.objects.filter(username=username).exists():
                errors['username'] = 'That username is already taken.'
            elif User.objects.filter(email=email).exists():
                errors['email'] = "You've already registered with that e-mail address."

            return len(errors) == 0

        if check_credentials():
            if ' ' in real_name:
                first_name, last_name = re.split(r'\s+', real_name, maxsplit=1)
            else:
                first_name, last_name = real_name, ''

            user = User(username=username, first_name=first_name,
                        last_name=last_name, email=email)
            user.set_password(password)
            user.save()

            email_body = render_to_string('mail/verify.txt', {
                'verify_url': '%(base_url)s?confirmation_key=%(key)s' % {
                    'base_url': request.build_absolute_uri(
                        reverse('verify', kwargs={'username': user.username})),
                    'key': user.confirmation_key
                }
            })
            send_mail('Verify your e-mail address', email_body,
                      f'Box Office Hawk <{settings.DEFAULT_FROM_EMAIL}>',
                      [user.email])

            messages.info(request, ("We've sent you a verification e-mail. "
                                    "Check your inbox."))
            return redirect('index')

    return render(request, 'register.html', {
        'username': username,
        'real_name': real_name,
        'email': email,
        'errors': errors
    }, status=400 if errors else 200)


def verify(request, username):
    user = get_object_or_404(User, username=username)

    confirmation_key = request.GET.get('confirmation_key')
    if not confirmation_key:
        messages.error(request, "That verification code isn't any good.")
        return redirect('index')

    try:
        user.confirm_email(confirmation_key)
    except CannotConfirmEmail:
        messages.error(request, "That verification code isn't any good.")
    else:
        login_user(request, user,
                   backend='django.contrib.auth.backends.ModelBackend')
        messages.success(request, f'Welcome to the site, {user.username}!')

    return redirect('index')


def profile(request):
    return render(request, 'profile.html', {
        'matchups_url': request.build_absolute_uri(reverse('api:matchups')),
    })
