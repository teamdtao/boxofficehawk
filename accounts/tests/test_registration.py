from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from accounts.models import User


class UserRegistrationTestCase(TestCase):
    def test_happy_path(self):
        response = self.client.post(reverse('register'), data={
            'username': 'joe',
            'email': 'joe@example.com',
            'password': 'foo',
            'password_confirmation': 'foo'
        })

        self.assertRedirects(response, settings.LOGIN_REDIRECT_URL)

        user = User.objects.get(username='joe')
        self.assertEqual('joe@example.com', user.email)

        # Password should be hashed, not plain text!
        self.assertNotEqual('foo', user.password)
        self.assertEqual('pbkdf2_sha256',
                         user.password[:user.password.index('$')])

    def test_username_is_required(self):
        response = self.client.post(reverse('register'), data={
            'email': 'joe@example.com',
            'password': 'foo',
            'password_confirmation': 'foo'
        })

        self.assertEqual(400, response.status_code)
        self.assertIn('username', response.context['errors'])
