from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from simple_email_confirmation.models import SimpleEmailConfirmationUserMixin

from accounts.exceptions import CannotConfirmEmail


class User(SimpleEmailConfirmationUserMixin, AbstractUser):
    featured = models.BooleanField(default=False)

    def confirm_email(self, confirmation_key):
        try:
            return super().confirm_email(confirmation_key)
        except ObjectDoesNotExist:
            raise CannotConfirmEmail
