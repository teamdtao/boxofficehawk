from bomojo.backends import AbstractUserBackend


class UserBackend(AbstractUserBackend):
    def is_featured_contributor(self, user):
        return user.featured
