"""
accounts URL Configuration
"""

from django.conf.urls import url

from accounts import views

urlpatterns = [
    url(r'^login/', views.login, name='login'),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^register/', views.register, name='register'),
    url(r'^verify/(?P<username>\w+)/', views.verify, name='verify'),
    url(r'^profile/$', views.profile, name='profile'),
]
