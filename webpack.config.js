const path = require('path');

module.exports = {
  entry: {
    layout: path.resolve(__dirname, 'static/js/layout.js'),
    index: path.resolve(__dirname, 'static/js/index.js'),
    compare: path.resolve(__dirname, 'static/js/compare.js'),
    matchup: path.resolve(__dirname, 'static/js/matchup.js'),
    profile: path.resolve(__dirname, 'static/js/profile.js'),
    embed: path.resolve(__dirname, 'static/js/embed.js')
  },
  output: {
    path: path.resolve(__dirname, 'staticfiles'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.html$/,
        use: [
          'mustache-loader'
        ]
      }
    ]
  }
};
