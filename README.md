# Box Office Hawk

Compare movies based on box office performance.

## What it is

A simple web app that lets you search for movies and see their daily box office results charted together.

## How it works

Nothing fancy. The backend is a Django app using the [bomojo API][1] and the front end is basically a bunch of vanilla JavaScript stitching together a bunch of external libraries.

## Getting started

Set up a Postgres database:


```
createuser -d -E -R -S boxofficehawk
createdb -O boxofficehawk boxofficehawk
```

Install requirements:

```
pip install -r requirements.txt
npm install
```

## Running it yourself

You can run the application using something like [foreman][2] or [honcho][3], i.e.:

```
honcho start -f Procfile-dev
```

Note that a different Procfile is used for production.

## Configuration

The following features are configurable via environment variables:

### PORT

Exactly what you'd think. Defaults to 5000.

### GA_TRACKING_ID

Got a Google Analytics tracking ID? Great! You can supply this and the app will include the necessary JavaScript snippet.

### SHARETHIS_PROPERTY_ID

Specify a ShareThis ID to enable social sharing buttons for Facebook, Twitter, and Reddit.

### SENTRY_DSN

Provide a Sentry DSN for exception logging.

[1]: https://bitbucket.org/teamdtao/bomojo
[2]: https://github.com/ddollar/foreman
[3]: https://github.com/nickstenning/honcho
