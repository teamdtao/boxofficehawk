import json

from django import template

from bomojo.utils import get_avatar_url

register = template.Library()


@register.filter('json')
def messages_to_json(messages):
    return json.dumps([{
        'message': m.message,
        'tags': m.tags
    } for m in messages])


@register.filter('avatar')
def avatar_url(user, size=32):
    return get_avatar_url(user.email, size=size)
