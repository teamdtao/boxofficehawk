import base64
import io
import mimetypes
import re
import sys

from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.http import require_http_methods
from django.urls import reverse

from bomojo.matchups.models import Matchup
from bomojo.matchups.renderers import render_matchup
from boxofficehawk.models import MatchupDisplay


def index(request):
    return render(request, 'index.html', {
        'featured_matchups_url': request.build_absolute_uri(
            reverse('featured-matchups')),
    })


def compare(request):
    return render(request, 'compare.html', {
        'create_matchup_url': request.build_absolute_uri(
            reverse('api:matchups')),
    })


def matchup(request, slug):
    context = {
        'slug': slug,
        'matchup_url': request.build_absolute_uri(
            reverse('api:matchup', kwargs={'slug': slug})),
        'upload_url': request.build_absolute_uri(
            reverse('matchup-image', kwargs={'slug': slug})),
        'embed_url': request.build_absolute_uri(
            reverse('embed-chart', kwargs={'slug': slug})) + '?instructions=true',
        'allow_sharing': True,
    }

    # TODO: get rid of this code! (This logic should live in the bomojo
    # project.)
    matchup = get_object_or_404(Matchup, slug=slug)

    try:
        context['image_url'] = matchup.display.banner_full.url
        context['layout'] = matchup.display.layout
    except MatchupDisplay.DoesNotExist:
        context['image_url'] = matchup.image.url if matchup.image else None
        context['layout'] = 'original'

    return render(request, 'matchup.html', context)


def featured_matchups(request):
    # TODO: get rid of this view! (This should be in the bomojo project. It's
    # just here for now because it relies on the MatchupDisplay model, which
    # bomojo doesn't know about.)
    matchups = Matchup.objects.select_related('display').filter(featured=True)
    recent_matchups = matchups.order_by('-created_on')[:20]
    return JsonResponse([_render_featured_matchup(matchup)
                         for matchup in recent_matchups], safe=False)


@xframe_options_exempt
def embed_chart(request, slug):
    params = request.GET.copy()
    include_instructions = params.pop('instructions', 'false') != 'false'
    embed_url = request.build_absolute_uri(
            reverse('embed-chart', kwargs={'slug': slug}))

    if params:
        embed_url += '?' + params.urlencode()

    context = {
        'matchup_url': request.build_absolute_uri(
            reverse('api:matchup', kwargs={'slug': slug})),
        'matchup_view_url': request.build_absolute_uri(
            reverse('matchup', kwargs={'slug': slug})),
        'include_instructions': include_instructions,
        'embed_url': embed_url,
        'allow_sharing': True,
    }

    return render(request, 'embed.html', context)


@require_http_methods(['POST'])
def matchup_image(request, slug):
    matchup = get_object_or_404(Matchup, slug=slug)

    if matchup.user != request.user:
        return HttpResponseBadRequest("You can't change someone else's "
                                      "matchup image!")

    # request.body is bytes
    prefix, data = request.body.split(b',', 1)

    # decode prefix to a string so we can use a regular expression on it
    prefix = prefix.decode(request.encoding)

    prefix_parts = re.match(r'^data:(?P<content_type>\w+/\w+);base64$', prefix)
    if prefix_parts is None:
        return HttpResponseBadRequest('data is not a valid data URI')

    content_type = prefix_parts.group('content_type')

    try:
        decoded_bytes = base64.b64decode(data)
    except ValueError:
        return HttpResponseBadRequest('data should be base64-encoded')

    file_args = {
        'field_name': 'file',
        'name': '%(slug)s%(ext)s' % {
            'slug': matchup.slug,
            'ext': mimetypes.guess_extension(content_type) or ''
        },
        'content_type': content_type,
        'size': len(decoded_bytes),
        'charset': None
    }

    display = getattr(matchup, 'display', None) or MatchupDisplay(matchup=matchup)
    display.banner_full = InMemoryUploadedFile(io.BytesIO(decoded_bytes), **file_args)
    display.banner_medium = InMemoryUploadedFile(io.BytesIO(decoded_bytes), **file_args)
    display.banner_small = InMemoryUploadedFile(io.BytesIO(decoded_bytes), **file_args)
    display.save()

    return redirect('matchup', slug=slug)


def _render_featured_matchup(matchup):
    rendered = render_matchup(matchup, include_movies=False)

    try:
        display = matchup.display
        rendered['image_url_medium'] = display.banner_medium.url
        rendered['image_url_small'] = display.banner_small.url
    except MatchupDisplay.DoesNotExist:
        rendered['image_url_medium'] = rendered['image_url']
        rendered['image_url_small'] = rendered['image_url']

    return rendered
