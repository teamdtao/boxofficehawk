from django.db import models

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill


class MatchupDisplay(models.Model):
    class Layout:
        # The OG layout: commentary (description) on the left, whole bunch of
        # charts on the right
        ORIGINAL = 'original'

        # Custom layout: commentary with charts interspersed. Charts are
        # embedded via links to their embed URLs, rendered inline.
        CUSTOM = 'custom'

        CHOICES = (
            (ORIGINAL, 'Original'),
            (CUSTOM, 'Custom')
        )

    matchup = models.OneToOneField('matchups.Matchup', related_name='display')
    banner_full = models.ImageField(upload_to='matchup-banners/full/',
                                    null=False)
    banner_medium = ProcessedImageField(upload_to='matchup-banners/medium/',
                                        processors=[ResizeToFill(1110, 300)],
                                        format='JPEG', options={'quality': 95})
    banner_small = ProcessedImageField(upload_to='matchup-banners/small/',
                                       processors=[ResizeToFill(540, 140)],
                                       format='JPEG', options={'quality': 95})
    layout = models.CharField(max_length=128, choices=Layout.CHOICES, default=Layout.ORIGINAL)

    def __str__(self):
        return f'{self.matchup} ({self.layout})'
