from django.conf import settings


def common_settings(request):
    return {
        'app_version': settings.APP_VERSION,
        'ga_tracking_id': settings.GA_TRACKING_ID,
        'sharethis_property_id': settings.SHARETHIS_PROPERTY_ID,
    }
