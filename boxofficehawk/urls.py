"""
boxofficehawk URL Configuration
"""

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from boxofficehawk import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^account/', include('accounts.urls')),
    url(r'^compare/$', views.compare, name='compare'),
    url(r'^matchup/(?P<slug>[^/]+)/$', views.matchup, name='matchup'),
    url(r'^matchup/(?P<slug>[^/]+)/embed-chart/$', views.embed_chart,
        name='embed-chart'),
    url(r'^matchup/(?P<slug>[^/]+)/upload-image/$', views.matchup_image,
        name='matchup-image'),
    url(r'^matchups/$', views.featured_matchups, name='featured-matchups'),
    url(r'^api/', include('bomojo.urls', namespace='api')),
    url(r'^admin/', admin.site.urls),
    url(r'', include('social_django.urls', namespace='social')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
