from django.conf import settings
from django.urls import reverse

from bomojo.tests import TestCase

from accounts.models import User


class UrlsTestCase(TestCase):
    def test_matchup_urls(self):
        self.assertEqual(reverse('matchup', kwargs={
            'slug': 'my-matchup'
        }), '/matchup/my-matchup/')

        self.assertEqual(reverse('api:matchup', kwargs={
            'slug': 'my-matchup'
        }), '/api/matchups/my-matchup')
