from boxofficehawk.settings import *

SECRET_KEY = 'super-top-secret-key'

DATABASES = {
    'default': dj_database_url.config(
        default='postgres://postgres@127.0.0.1:5432/boxofficehawk_test')
}
