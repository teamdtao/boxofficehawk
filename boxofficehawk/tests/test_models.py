from django.conf import settings
from django.urls import reverse

from bomojo.tests import TestCase

from accounts.models import User
from boxofficehawk.models import MatchupDisplay


class MatchupDisplayTestCase(TestCase):
    def test_str(self):
        # TODO: fix create_user helper method in bomojo
        mike = User(username='mike', first_name='Mike',
                    email='mike@example.com')
        mike.set_password('top secret')
        mike.save()

        matchup = self.create_matchup(mike, ['Punch-Out!',
                                             'Super Punch-Out!!'])
        display = MatchupDisplay(matchup=matchup, layout='custom')
        display.save()

        self.assertEqual('mike/punch-out-vs-super-punch-out (custom)',
                         str(display))
