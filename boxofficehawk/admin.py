# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from boxofficehawk.models import MatchupDisplay


@admin.register(MatchupDisplay)
class MatchupDisplayAdmin(admin.ModelAdmin):
    list_display = ('matchup_title', 'matchup_user', 'layout')
    list_select_related = ('matchup',)

    def matchup_title(self, display):
        return display.matchup.title

    def matchup_user(self, display):
        return display.matchup.user

    matchup_title.short_description = 'Matchup'
    matchup_user.short_description = 'User'
