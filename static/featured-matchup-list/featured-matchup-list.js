import errors from '../js/errors';
import formatDate from '../js/format-date';
import makeRequest from '../js/make-request';

import './featured-matchup-list.scss';

import featuredMatchupListTemplate from './featured-matchup-list.html';

function FeaturedMatchupList(container, options) {
    this.container = container;
    this.url = container.getAttribute('data-matchups-url');
}

FeaturedMatchupList.prototype.loadMatchups = function loadMatchups() {
    var container = this.container;

    makeRequest.get(this.url).then(
        function onFulfilled(matchups) {
            container.innerHTML = featuredMatchupListTemplate({
                matchups: matchups.map(function(matchup) {
                    return {
                        creator: matchup.creator,
                        title: matchup.title,
                        imageUrl: getImageUrl(matchup),
                        slug: matchup.slug,
                        created: formatDate(new Date(matchup.created))
                    };
                })
            });
        },
        function onRejected(result) {
            errors.add(result.errors);
        });
};

function getImageUrl(matchup) {
    var width = screen.width || window.innerWidth || document.body.clientWidth;
    if (width < 768) {
        return matchup.image_url_small;
    }
    return matchup.image_url_medium;
}

export default FeaturedMatchupList;
