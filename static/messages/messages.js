import utils from '../js/utils';

import './message-list.scss';

import messageTemplate from './message-list.html';

function MessageStore() {
    this.messages = [];
}

MessageStore.prototype.initialize = function initialize(element) {
    var initialMessages = element.getAttribute('data-messages');
    if (initialMessages) {
        initialMessages = JSON.parse(initialMessages);
        this.messages.push.apply(this.messages, initialMessages);
    }
    this.messageList = new MessageList(this, element);
};

MessageStore.prototype.add = function add(message) {
    if (!this.hasMessage(message)) {
        this.messages.push(message);
    }
    this.messageList._render();
};

MessageStore.prototype.getMessages = function getMessages() {
    return this.messages.slice(0);
};

MessageStore.prototype.hasMessage = function hasMessage(message) {
    for (var i = 0; i < this.messages.length; ++i) {
        if (this.messages[i].message == message.message) {
            return true;
        }
    }
    return false;
};

MessageStore.prototype.clear = function clear(tags) {
    if (tags) {
        utils.removeWhere(this.messages, function(message) {
            return message.tags == tags;
        });
    } else {
        this.messages.splice(0, this.messages.length);
    }
    this.messageList._render();
};

function MessageList(messageStore, element) {
    this.messageStore = messageStore;
    this.element = element;
    this._render();
}

MessageList.prototype._render = function _render() {
    var messageStore = this.messageStore;
    this.element.innerHTML = messageTemplate({
        messages: messageStore.getMessages()
    });
};

// TODO: Is this really necessary?
if (!window.__messages__) {
    window.__messages__ = new MessageStore();
}

export default window.__messages__;
