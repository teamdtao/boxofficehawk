import $ from 'jquery';
import Bloodhound from 'corejs-typeahead';

import config from '../js/config';

function MovieSearchWidget(element) {
    var self = this;

    var moviesSearch = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: config.getApiHost() + '/movies/search?title=%TITLE',
            wildcard: '%TITLE',
            transform: function(response) {
                return response.results;
            }
        }
    });

    var $element = $(element);

    $element.typeahead({}, {
        source: moviesSearch,
        display: 'title',
        limit: 10
    });

    $element.bind('typeahead:select', function(e, value) {
        self.addMovie(value);
        $element.typeahead('val', '');
    });

    this.movies = [];

    this.moviesListElement = document.createElement('div');
    this.moviesListElement.setAttribute('class', 'movies-list');
    element.parentNode.insertBefore(this.moviesListElement,
        element.nextSibling);

    this.moviesChangedCallback = null;
}

MovieSearchWidget.prototype.populate = function populate(movies) {
    this.movies.splice(0);
    this.movies.push.apply(this.movies, movies);
    this._renderMovies();
};

MovieSearchWidget.prototype.addMovie = function addMovie(movie) {
    if (this._movieAlreadyAdded(movie)) {
        return;
    }
    this.movies.push(movie);
    this._renderMovie(movie);
    this._moviesChanged();
};

MovieSearchWidget.prototype.getMovies = function getMovies() {
    return this.movies;
};

MovieSearchWidget.prototype.getMovieIds = function getMovieIds() {
    return this.movies.map(function(movie) { return movie.movie_id; });
};

MovieSearchWidget.prototype.removeMovie = function removeMovie(movie) {
    var index = this.movies.indexOf(movie);
    if (index != -1) {
        this.movies.splice(index, 1);
    }
    this._renderMovies();
    this._moviesChanged();
};

MovieSearchWidget.prototype.onMoviesChanged = function onMoviesChanged(callback) {
    this.moviesChangedCallback = callback;
};

MovieSearchWidget.prototype._renderMovies = function _renderMovies() {
    var self = this;

    this.moviesListElement.innerHTML = '';
    this.movies.forEach(function(movie) {
        self._renderMovie(movie);
    });
};

MovieSearchWidget.prototype._renderMovie = function _renderMovie(movie) {
    var self = this;

    var tag = document.createElement('span');
    tag.setAttribute('class', 'badge badge-pill badge-primary');
    tag.setAttribute('data-movie-id', movie.movie_id);
    tag.textContent = movie.title;

    var removeButton = document.createElement('button');
    removeButton.setAttribute('class', 'close');
    removeButton.innerHTML = '&times;';
    tag.appendChild(removeButton);

    removeButton.addEventListener('click', function(e) {
        e.preventDefault();
        self.removeMovie(movie);
    });

    this.moviesListElement.appendChild(tag);
};

MovieSearchWidget.prototype._moviesChanged = function _moviesChanged() {
    if (this.moviesChangedCallback) {
        this.moviesChangedCallback();
    }
};

MovieSearchWidget.prototype._movieAlreadyAdded = function _movieAlreadyAdded(movie) {
    for (var i = 0; i < this.movies.length; ++i) {
        if (this.movies[i].movie_id == movie.movie_id) {
            return true;
        }
    }

    return false;
};

export default MovieSearchWidget;
