import marked from 'marked';

import charts from '../chart/charts';
import config from '../js/config';
import errors from '../js/errors';
import makeRequest from '../js/make-request';
import relativeDate from '../js/relative-date';
import renderMatchupChart from '../js/render-matchup-chart';

import './matchup-container.scss';

import matchupHeaderTemplate from './matchup-header.html';
import matchupDetailsTemplate from './matchup-details.html';

function MatchupContainer(element) {
    this.element = element;
    this.url = element.getAttribute('data-matchup-url');
    this.imageUrl = element.getAttribute('data-image-url');
    this.embedUrl = element.getAttribute('data-embed-url');
}

MatchupContainer.prototype.loadMatchup = function loadMatchup() {
    var element = this.element,
        imageUrl = this.imageUrl,
        embedUrl = this.embedUrl,
        headerElement = this.element.querySelector('.matchup-banner'),
        detailsElement = this.element.querySelector('.matchup-details'),
        sharingButtons = this.element.querySelector('.sharethis-inline-share-buttons'),
        descriptionElement = this.element.querySelector('.matchup-description');

    return makeRequest.get(this.url).then(
        function onFulfilled(matchup) {
            headerElement.style.backgroundImage = 'url("' + (imageUrl || matchup.image_url) + '")';
            headerElement.style.backgroundSize = 'cover';

            headerElement.innerHTML = matchupHeaderTemplate({
                title: matchup.title,
                url: window.location,
                embedUrl: embedUrl,
                canEdit: matchup.creator.username == config.getCurrentUser()
            });

            detailsElement.innerHTML = matchupDetailsTemplate({
                creator: matchup.creator,
                created: relativeDate(matchup.created),
                updated: relativeDate(matchup.updated)
            });

            if (sharingButtons) {
                sharingButtons.setAttribute('data-title', matchup.title);
            }

            descriptionElement.innerHTML = marked(matchup.description);

            var chartLinks = descriptionElement.querySelectorAll(
                'a[href*="/embed-chart/"],a[href*="/compare/"]');

            chartLinks.forEach(function(chartLink) {
                // Only render charts for links that stand on their own,
                // i.e. not links in the middle of a text paragraph.
                if (chartLink.parentNode.children.length > 1) {
                    return;
                }

                renderMatchupChart.fromQuery(
                    chartLink.parentNode, matchup, chartLink.getAttribute('href'));
            });

            return matchup;
        }, function onRejected(result) {
            errors.add(result.errors);
            return Promise.reject(result.errors);
        });
}

export default MatchupContainer;
