import errors from '../js/errors';
import makeRequest from '../js/make-request';
import relativeDate from '../js/relative-date';

import './matchup-list.scss';

import matchupListTemplate from './matchup-list.html';

function MatchupList(container, options) {
    this.container = container;
    this.url = container.getAttribute('data-matchups-url');
}

MatchupList.prototype.loadMatchups = function loadMatchups() {
    var container = this.container;

    makeRequest.get(this.url).then(
        function onFulfilled(matchups) {
            container.innerHTML = matchupListTemplate({
                matchups: matchups.map(function(matchup) {
                    return {
                        creator: matchup.creator,
                        title: matchup.title,
                        slug: matchup.slug,
                        featured: matchup.featured,
                        created: relativeDate(matchup.created),
                        updated: relativeDate(matchup.updated)
                    };
                })
            });
        },
        function onRejected(result) {
            errors.add(result.errors);
        });
};

export default MatchupList;
