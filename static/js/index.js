import FeaturedMatchupList from '../featured-matchup-list/featured-matchup-list';

document.addEventListener('DOMContentLoaded', function() {
    new FeaturedMatchupList(document.querySelector(
        '#featured-matchups .matchup-list')).loadMatchups();
});
