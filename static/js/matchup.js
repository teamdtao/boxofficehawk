import $ from 'jquery';
import 'cropit';

import config from './config';
import EditMatchupForm from '../edit-matchup-form/edit-matchup-form';
import loadAllMovies from './load-all-movies';
import makeRequest from './make-request';
import MatchupContainer from '../matchup-container/matchup-container';
import parsePeriod from './parse-period';
import renderCharts from './render-charts';

document.addEventListener('DOMContentLoaded', function() {
    var editMatchupForm,
        uploadImageForm = document.getElementById('upload-image-form');

    // actual stuff that happens
    initializeUI();
    loadMatchup();

    // implementation of everything
    function initializeUI() {
        $('.image-editor').cropit({
            imageBackground: true,
            imageBackgroundBorderWidth: 20,
            exportZoom: 2
        });

        editMatchupForm = new EditMatchupForm(document.getElementById('edit-form'));
        editMatchupForm.initialize();

        uploadImageForm.addEventListener('submit', function(e) {
            e.preventDefault();

            var imageData = $('.image-editor').cropit('export');

            var xhr = new XMLHttpRequest();
            xhr.open('POST', uploadImageForm.getAttribute('action'));
            xhr.addEventListener('load', function() {
                if (xhr.status != 200) {
                    // displayErrors({ 'data': xhr.responseText });
                    console.error(xhr.responseText);
                    return;
                }

                window.location.reload();
            });

            xhr.send(imageData);
        });
    }

    function loadMatchup() {
        document.body.classList.add('loading-matchup');
        var matchupContainer = new MatchupContainer(document.querySelector('.matchup'));
        matchupContainer.loadMatchup().then(function(matchup) {
            loadAllMovies(matchup.movies).then(function(args) {
                var results = args.results,
                    dayOffset = args.dayOffset;

                displayMatchup(matchup, results, dayOffset);
                document.body.classList.remove('loading-matchup');
            });
        });
    }

    function displayMatchup(matchupData, results, dayOffset) {
        if (matchupData.creator.username == config.getCurrentUser()) {
            document.body.setAttribute('data-editable', 'true');
        } else {
            document.body.removeAttribute('data-editable');
        }

        config.setTitle(matchupData.title);

        editMatchupForm.showMatchup(matchupData);

        renderCharts(results, {
            count: parsePeriod(matchupData.period, dayOffset)
        });

        // HACK: Previously this code was using on('shown.bs.modal') but that
        // doesn't work unless this file imports bootstrap, which I'm trying to
        // avoid (see 1538e04 for context).
        var editButton = document.getElementById('edit-button');
        if (editButton) {
            editButton.addEventListener('click', function(e) {
                setTimeout(function() {
                    editMatchupForm.refreshEditor();
                }, 0);
            });
        }
    }

});
