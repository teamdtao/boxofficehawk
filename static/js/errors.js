import messages from '../messages/messages';

function Errors() {
}

Errors.prototype.add = function add(errors) {
    if (!Array.isArray(errors)) {
        errors = [errors]
    }

    errors.forEach(function(error) {
        messages.add({
            tags: 'danger',
            message: error
        });
    });

    document.body.setAttribute('data-errors', true);
};

Errors.prototype.clear = function clear() {
    messages.clear('danger');
    document.body.removeAttribute('data-errors');
};

const errors = new Errors();

export default errors;
