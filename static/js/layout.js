import 'bootstrap';

import messages from '../messages/messages';

import '../css/index.scss';

document.addEventListener('DOMContentLoaded', function() {
    messages.initialize(document.getElementById('message-list'));
});
