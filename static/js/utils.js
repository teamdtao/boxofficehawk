export default {
  arrayCompare: function arrayCompare(array, other) {
    if (array.length != other.length) {
      return false;
    }

    for (var i = 0; i < array.length; ++i) {
      if (array[i] !== other[i]) {
        return false;
      }
    }

    return true;
  },

  compact: function compact(values) {
    return values.filter(function(value) {
      return !!value;
    });
  },

  merge: function merge(a, b) {
    var merged = {};
    for (var k in a) {
      merged[k] = a[k];
    }
    for (var k in b) {
      if (typeof merged[k] === 'object' && typeof b[k] === 'object') {
        merged[k] = merge(merged[k], b[k]);
      } else {
        merged[k] = b[k];
      }
    }
    return merged;
  },

  query: {
    create: function queryCreate(data) {
      var query = Object.keys(data).map(function(key) {
        switch (key) {
          case 'movies':
            return 'movies=' + data[key].map(function(movie) {
              return encodeURIComponent(movie.movie_id);
            }).join(',');
          case 'adjusted':
            return 'adjusted=' + (data[key] ? '1' : '0');
          default:
            return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
        }
      });

      return '?' + query.join('&');
    },

    parse: function queryParse(querystring) {
      var query = {};

      querystring.slice(querystring.indexOf('?') + 1).split('&').forEach(function(param) {
        var pair = param.split('=');
        switch (pair[0]) {
          case 'movies':
            query.movies = pair[1].split(',').map(decodeURIComponent);
            break;
          case 'adjusted':
            query.adjusted = (pair[1] != '0');
            break;
          default:
            query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
            break;
        }
      });

      return query;
    }
  },

  removeWhere: function removeWhere(array, condition) {
    var removed = [];

    for (var i = array.length - 1; i >= 0; --i) {
        if (condition(array[i])) {
            removed.push(array[i]);
            array.splice(i, 1);
        }
    }

    return removed;
  }
};
