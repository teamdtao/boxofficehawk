import charts from '../chart/charts';
import loadAllMovies from './load-all-movies';
import parsePeriod from './parse-period';
import utils from './utils';


function renderMatchupChart(element, matchup, params) {
    var chartType = params.chart || 'cumulative',
        Chart = charts[chartType];

    if (!Chart) {
        throw Error('unknown chart type: "' + chartType + '"');
    }

    var period = params.period || matchup.period,
        periodStart = params.periodStart || '0d',
        movies = filterMatchup(matchup, params.movies),
        prediction = params.prediction;

    return loadAllMovies(movies, params.adjusted).then(function(args) {
        var results = args.results,
            dayOffset = args.dayOffset;

        // This data is used to include a link back to the matchup.
        // It's intended for the embed view.
        var matchupData;
        if (document.body.hasAttribute('data-matchup-view-url')) {
            matchupData = {
                url: document.body.getAttribute('data-matchup-view-url'),
                title: matchup.title
            };
        }

        var chart = new Chart(element, matchupData);

        // Only apply an offset (to account for a non-traditional opening like
        // on a Wednesday) if starting from the beginning. E.g. if I want the
        // first week but the film opened on a Wednesday, then I _actually_
        // want the first 9 days.
        var offset = periodStart ? 0 : dayOffset;

        chart.render(results, {
            start: parsePeriod(periodStart),
            count: parsePeriod(period, offset),
            prediction: prediction
        });
        return chart;
    });
}

renderMatchupChart.fromQuery = function fromQuery(element, matchup, query) {
    var params = utils.query.parse(query);
    return renderMatchupChart(element, matchup, params);
}

function filterMatchup(matchup, movies) {
    if (!movies) {
        return matchup.movies;
    }

    return matchup.movies.filter(function(movie) {
        return movies.indexOf(movie.movie_id) != -1;
    });
}

export default renderMatchupChart;
