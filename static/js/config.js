const TITLE_BASE = 'Box Office Hawk';

module.exports = {
  getApiHost: function() {
    return '/api';
  },

  getCurrentUser: function() {
    return document.body.getAttribute('data-username') || '';
  },

  setTitle: function(title) {
    if (title) {
      title = TITLE_BASE + ' - ' + title;
    } else {
      title = TITLE_BASE;
    }

    document.title = title;
  }
};
