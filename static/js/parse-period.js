/**
 * Parses a string representing a time period and returns the number of days
 * that period represents.
 *
 * For example "1w" represents 1 week and "10d" represents 10 days.
 *
 * This function accepts a conditional offset which should only be applied for
 * non-null periods. (A period of null represents an indefinite period.)
 */
function parsePeriod(period, offset) {
    if (!period) {
        return null;
    }

    try {
        var parsed = period.match(/^(\d+)([dw])$/),
            value = Number(parsed[1]),
            unit = parsed[2];
    } catch (e) {
        return null;
    }

    switch (unit) {
        case 'w':
            value *= 7;
        case 'd':
        default:
            break;
    }

    return value + (offset || 0);
}

export default parsePeriod;
