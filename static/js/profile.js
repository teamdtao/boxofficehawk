import MatchupList from '../matchup-list/matchup-list';

document.addEventListener('DOMContentLoaded', function() {
    new MatchupList(document.querySelector(
        '#my-matchups .matchup-list')).loadMatchups();
});
