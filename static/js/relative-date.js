import formatDate from './format-date';

const SECOND = 1000;
const MINUTE = SECOND * 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;
const WEEK = DAY * 7;

function relativeDate(date) {
    if (!(date instanceof Date)) {
        date = new Date(date);
    }

    const now = Date.now();
    const timestamp = date.getTime();

    if ((now - timestamp) < MINUTE) {
        return 'just now';
    }

    var value, unit;
    if ((now - timestamp) < HOUR) {
        value = Math.floor((now - timestamp) / MINUTE);
        unit = value == 1 ? 'minute' : 'minutes';
    }

    else if ((now - timestamp) < DAY) {
        value = Math.floor((now - timestamp) / HOUR);
        unit = value == 1 ? 'hour' : 'hours';
    }

    else if ((now - timestamp) < WEEK) {
        value = Math.floor((now - timestamp) / DAY);
        if (value == 1) {
            return 'yesterday';
        }
        unit = 'days';
    }

    else if ((now - timestamp) < (WEEK * 4)) {
        value = Math.floor((now - timestamp) / WEEK);
        unit = value == 1 ? 'week' : 'weeks';
    }

    if (unit) {
        return [value, unit, 'ago'].join(' ');
    }

    return formatDate(date);
}

export default relativeDate;
