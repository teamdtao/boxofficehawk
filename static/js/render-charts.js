import charts from '../chart/charts';

function renderCharts(results, options) {
    const chartTypes = [
        'daily',
        'cumulative',
        'multiple',
        'theaters',
        'per-theater',
        'weekly-change'
    ];

    chartTypes.forEach(function(chartType) {
        var Chart = charts[chartType],
            element = document.getElementById(chartType + '-results');

        if (!element) {
            return;
        }

        new Chart(element).render(results, options);
    });
}

export default renderCharts;
