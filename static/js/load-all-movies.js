import errors from './errors';
import getBoxOffice from './get-box-office';
import utils from './utils';

const DAYS = ['Fri', 'Thu', 'Wed', 'Tue', 'Mon', 'Sun', 'Sat'];

function loadAllMovies(movies, adjusted) {
    if (movies.length == 0) {
        return Promise.resolve({
            results: [],
            dayOffset: 0
        });
    }

    var requests = movies.map(function(movie) {
        return getBoxOffice(movie, adjusted).then(
            function onFulfilled(result) {
                // When landing on a page directly from a URL with encoded movie IDs, the IDs will have
                // been used temporarily as the titles since the actual titles weren't known.
                movie.title = result.title;
                return result;
            },
            function onRejected(errs) {
                errors.add(errs);
            });
    });

    return Promise.all(requests).then(function(results) {
        // Filter out undefined from movies whose box office results couldn't
        // be loaded.
        results = utils.compact(results);

        var dayOffset = alignResults(results);

        return {
            results: results,
            dayOffset: dayOffset
        };
    });
}

/**
 * Given an Array of box office results, pads results with blank data as
 * necessary to ensure all first Fridays are aligned.
 */
function alignResults(results) {
    var dayOffset = getDayOffset(results);

    results.forEach(function(result) {
        padBoxOffice(result, dayOffset);
    });

    return dayOffset;
}

/**
 * Given an Array of box office results, computes the maximum number of days by
 * which any of the results must be offset so that all first Fridays are
 * aligned.
 *
 * For example, if all movies in a set of results opened on a Friday except for
 * one which opened on a Wednesday, this function would return 2.
 */
function getDayOffset(results) {
    var maxDayOffset = 0;

    results.forEach(function(result) {
        if (result.box_office.length == 0) {
            return;
        }
        maxDayOffset = Math.max(maxDayOffset, getDayIndex(result.box_office[0].day));
    });

    return maxDayOffset;
}

/**
 * Inserts "blank" data points at the start of a set of box office results if
 * necessary to align with a particular day offset.
 *
 * Also inserts blank data points for "missing" days, i.e. if a movie has a
 * debut and then doesn't come back to theaters until the following weekend.
 */
function padBoxOffice(result, dayOffset) {
    if (result.box_office.length == 0) {
        return;
    }

    var currentDay = getDayIndex(result.box_office[0].day);

    while (currentDay++ < dayOffset) {
        result.box_office.unshift({
            day: DAYS[currentDay],
            date: null,
            rank: null,
            gross: null,
            theaters: null,
            cumulative: null
        });
    }

    var index = 0,
        dayIndex = getDayIndex(result.box_office[index].day),
        expectedDay = dayIndex == 0 ? 6 : dayIndex - 1;

    for (index = 1; index < result.box_office.length; ++index) {
        if (getDayIndex(result.box_office[index].day) != expectedDay) {
            result.box_office.splice(index, 0, {
                day: DAYS[expectedDay],
                date: null,
                rank: null,
                gross: 0,
                theaters: 0,
                cumulative: result.box_office[index - 1].cumulative
            });
        }
        expectedDay = expectedDay == 0 ? 6 : expectedDay - 1;
    }
}

/**
 * A day's "index" is the number of days before Friday; Fri = 0, Thu = 1, etc.
 */
function getDayIndex(day) {
    return DAYS.indexOf(day.slice(0, 3));
}

export default loadAllMovies;
