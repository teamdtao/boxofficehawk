import config from './config';

function makeRequest(method, path, data) {
    var xhr = new XMLHttpRequest();

    // Prefix relative paths w/ configured API host (or root path).
    if (path.charAt(0) == '/') {
        path = config.getApiHost() + path;
    }

    xhr.open(method, path);

    if (['post', 'put'].indexOf(method.toLowerCase()) !== -1) {
        xhr.setRequestHeader('content-type', 'application/json');
        data = JSON.stringify(data);
    }

    return new Promise(function(resolve, reject) {
        xhr.addEventListener('load', function() {
            var responseData, errors;
            try {
                responseData = JSON.parse(xhr.responseText);
                if (responseData.errors) {
                    errors = flattenErrors(responseData.errors);
                }
            } catch (e) {
                // This is probably the result of the response being invalid
                // JSON (i.e. HTML), in which case a generic error message is
                // better than "SyntaxError: Unexpected token <" etc.
                errors = ['An unexpected error occurred.'];
            }
            if (errors) {
                reject({
                    status: xhr.status,
                    errors: errors
                });
            } else {
                resolve(responseData);
            }
        });

        xhr.addEventListener('error', function(e) {
            reject({
                status: null,
                errors: ['Error from ' + method + ' request to ' + path]
            });
        });

        xhr.send(data);
    });
}

function flattenErrors(errors) {
    var flattened = [];

    Object.keys(errors).forEach(function(key) {
        flattened.push.apply(flattened, errors[key].map(function(error) {
            return key + ': ' + error;
        }));
    });

    return flattened;
}

makeRequest.get = function get(path, retries) {
    if (typeof retries == 'undefined') {
        retries = 3;
    }

    return makeRequest('GET', path).catch(function(result) {
        if (retries > 0 && (result.status == null || result.status >= 500)) {
            return get(path, retries - 1);
        }

        return Promise.reject(result);
    });
};

makeRequest.post = function post(path, data) {
    return makeRequest('POST', path, data);
};

export default makeRequest;
