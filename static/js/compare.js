import config from './config';
import EditMatchupForm from '../edit-matchup-form/edit-matchup-form';
import errors from './errors';
import loadAllMovies from './load-all-movies';
import MovieSearchWidget from '../movie-search-widget/movie-search-widget';
import parsePeriod from './parse-period';
import renderCharts from './render-charts';
import utils from './utils';

document.addEventListener('DOMContentLoaded', function() {
    var searchForm = document.getElementById('search-form'),
        periodField = searchForm.querySelector('select[name="period"]'),

        saveButton = document.getElementById('save-button'),
        movieWidget, saveForm;

    // actual stuff that happens on page load
    loadMoviesFromQuery();
    initializeUI();
    initializeState();

    // all teh codez
    function loadMoviesFromQuery() {
        var params = utils.query.parse(location.search);

        // No need to load everything back up if the page already has a state
        // matching the querystring.
        if (history.state && compareState(history.state, params)) {
            return;
        }

        if (params.movies) {
            loadMovies(params.movies.map(function(movieId) {
                return {
                    movie_id: movieId,
                    title: movieId
                };
            }), params, true);
        }
    }

    function initializeUI() {
        movieWidget = new MovieSearchWidget(
            searchForm.querySelector('input[name="movies"]'));

        movieWidget.onMoviesChanged(function() {
            loadMovies(movieWidget.getMovies());
        });

        periodField.addEventListener('change', function() {
            loadMovies(movieWidget.getMovies());
        });

        searchForm.addEventListener('submit', function(e) {
            e.preventDefault();
        });

        saveForm = new EditMatchupForm(document.getElementById('save-form'));
        saveForm.initialize();

        // Prevent the save button from submitting the search form.
        saveButton.addEventListener('click', function(e) {
            e.preventDefault();

            saveForm.showMatchup({
                title: '',
                description: '',
                movies: movieWidget.getMovies(),
                period: periodField.value
            });
        });
    }

    function initializeState() {
        if (history.state) {
            displayState(history.state, true);
        }

        window.addEventListener('popstate', function(e) {
            if (!e.state.movies) {
                return;
            }

            displayState(e.state, true);
        });
    }

    function displayState(state, refresh) {
        if (refresh) {
            populateForm(state.movies, state.period);
        }
        renderCharts(state.results, {
            start: parsePeriod(state.periodStart),
            count: parsePeriod(state.period, state.dayOffset)
        });
        updateTitle(state.movies);
        if (state.movies.length > 0) {
            document.body.classList.add('showing-box-office');
        } else {
            document.body.classList.remove('showing-box-office');
        }
    }

    function compareState(state, params) {
        if (!state || !params) {
            return false;
        }

        return utils.arrayCompare(state.movies || [], params.movies || []) &&
            state.periodStart == params.periodStart &&
            state.period == params.period;
    }

    function loadMovies(movies, params, refresh) {
        errors.clear();

        params || (params = {});

        var periodStart = params.periodStart || '0d',
            period = params.period || periodField.value;

        document.body.classList.add('loading-box-office');
        loadAllMovies(movies).then(function(args) {
            var results = args.results,
                dayOffset = args.dayOffset;

            document.body.classList.remove('loading-box-office');
            displayState({
                movies: movies,
                periodStart: periodStart,
                period: period,
                dayOffset: dayOffset,
                results: results
            }, refresh);
            history.pushState({
                movies: movies,
                periodStart: periodStart,
                period: period,
                results: results
            }, '', utils.query.create({
                movies: movies,
                periodStart: periodStart,
                period: period
            }));
        });
    }

    function populateForm(movies, period) {
        movieWidget.populate(movies);
        periodField.value = period || '';
    }

    function updateTitle(movies) {
        var movieTitles = movies.map(function(movie) {
            return movie.title;
        });

        config.setTitle(movieTitles.join(' / '));
    }

    function getDayAtOrdinal(results, ordinal) {
        for (var i = 0; i < results.length; ++i) {
            if (ordinal > 0 && ordinal <= results[i].box_office.length) {
                return results[i].box_office[ordinal - 1].day;
            }
        }

        return null;
    }
});
