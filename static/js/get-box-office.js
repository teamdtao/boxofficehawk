import config from './config';
import makeRequest from './make-request';

function getBoxOffice(movie, adjusted) {
    // Normalize adjusted to 1 or 0 (for deterministic value in cache key).
    adjusted = adjusted ? 1 : 0;

    var localCacheKey = [movie.movie_id, adjusted].join('|');

    var promise = window.__boxoffice__[localCacheKey];
    if (!promise) {
        // TODO: get rid of this hard-coded URL pattern
        var path = '/movies/' + encodeURIComponent(movie.movie_id) + '/boxoffice';
        if (adjusted) {
            path += '?adjusted=1'
        }
        promise = makeRequest.get(path).catch(function() {
            delete window.__boxoffice__[localCacheKey];
            return Promise.reject(['No luck finding "' + movie.title + '" :(']);
        });
        window.__boxoffice__[localCacheKey] = promise;
    }

    return promise;
}

if (!window.__boxoffice__) {
    window.__boxoffice__ = {};
}

export default getBoxOffice;
