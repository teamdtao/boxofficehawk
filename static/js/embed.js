import charts from '../chart/charts';
import makeRequest from './make-request';
import renderMatchupChart from './render-matchup-chart';
import utils from './utils';

document.addEventListener('DOMContentLoaded', function() {
    var params = utils.query.parse(location.search),
        chartType = params.chart || 'cumulative',
        Chart = charts[chartType];

    if (!Chart) {
        throw Error('unknown chart type: "' + chartType + '"');
    }

    document.body.classList.add('loading-matchup');
    var getMatchup = makeRequest.get(
        document.body.getAttribute('data-matchup-url'));

    var renderChart = getMatchup.then(
        function(matchup) {
            return renderMatchupChart(
                document.getElementById('chart'), matchup, params);
        });

    renderChart.then(function() {
        document.body.classList.remove('loading-matchup');
    })
});
