import SimpleMDE from 'simplemde';

import errors from '../js/errors';
import makeRequest from '../js/make-request';
import MovieSearchWidget from '../movie-search-widget/movie-search-widget';

import 'simplemde/dist/simplemde.min.css';
import '../css/overrides.scss'

import editMatchupFormTemplate from './edit-matchup-form.html';

function EditMatchupForm(element) {
    this.element = element;
    this.url = element.getAttribute('action');
    this.method = element.getAttribute('method');
    this.creatingMatchup = this.method == 'POST';
    this.canFeature = element.getAttribute('data-featured-contributor');

    // populated in initialize()
    this.titleInput = null;
    this.periodField = null;
    this.featuredCheckbox = null;
    this.descriptionEditor = null;
    this.movieWidget = null;
}

EditMatchupForm.prototype.initialize = function initialize() {
    var self = this;

    this.element.innerHTML = editMatchupFormTemplate({
        creatingMatchup: this.creatingMatchup,
        canFeature: this.canFeature,
        saveButtonLabel: this.creatingMatchup ? 'Create' : 'Save'
    });

    this.titleInput = this.element.querySelector('input[name="title"]');
    this.periodField = this.element.querySelector('select[name="period"]');
    this.featuredCheckbox = this.element.querySelector('input[name="featured"]');

    this.descriptionEditor = new SimpleMDE({
        element: this.element.querySelector('textarea[name="description"]')
    });

    this.movieWidget = new MovieSearchWidget(
        this.element.querySelector('input[name="movies"]'));

    this.element.addEventListener('submit', function(e) {
        e.preventDefault();

        var properties = {
            title: self.titleInput.value,
            description: self.descriptionEditor.value(),
            movies: self.movieWidget.getMovieIds(),
            period: self.periodField.value
        };

        if (self.featuredCheckbox) {
            properties.featured = self.featuredCheckbox.checked;
        }

        document.body.classList.add('loading-save-matchup');
        makeRequest(self.method, self.url, properties).then(
            function onFulfilled(result) {
                document.body.classList.remove('loading-save-matchup');
                window.location = '/matchup/' + result.slug;
            },
            function onRejected(result) {
                errors.add(result.errors);
            });
    });
};

EditMatchupForm.prototype.showMatchup = function showMatchup(matchup) {
    this.titleInput.value = matchup.title;
    this.descriptionEditor.value(matchup.description);
    this.movieWidget.populate(matchup.movies);
    this.periodField.value = matchup.period;

    if (this.featuredCheckbox) {
        this.featuredCheckbox.checked = matchup.featured;
    }
};

EditMatchupForm.prototype.refreshEditor = function refreshEditor() {
    this.descriptionEditor.codemirror.refresh();
};

export default EditMatchupForm;
