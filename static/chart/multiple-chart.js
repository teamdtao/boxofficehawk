import BaseChart from './base-chart'

function MultipleChart() {
    BaseChart.apply(this, arguments);
}

MultipleChart.prototype = Object.create(BaseChart.prototype);

MultipleChart.prototype.processResults = function processResults(results) {
    var self = this;
    this.openingWeekends = results.map(function(result) {
        return self._getOpeningWeekend(result.box_office);
    });
};

MultipleChart.prototype.getTitleText = function getTitleText() {
    return 'Opening weekend multiple';
};

MultipleChart.prototype.getValue = function getValue(daily, index, boxOffice, resultIndex) {
    return daily.cumulative / this.openingWeekends[resultIndex];
};

MultipleChart.prototype._getOpeningWeekend = function _getOpeningWeekend(boxOffice) {
    var maxTheaters = 0;
    boxOffice.forEach(function(result) {
        maxTheaters = Math.max(maxTheaters, result.theaters);
    });

    var index = 0,
        total = 0,
        releasedWide = false;

    while (index < boxOffice.length) {
        // Let's say a movie's opening weekend is the first weekend it's
        // playing to at least 50% of its max theater count.
        if (boxOffice[index].theaters >= (maxTheaters * 0.5)) {
            releasedWide = true;
        }

        total += boxOffice[index].gross || 0;
        if (boxOffice[index++].day == 'Sun' && releasedWide) {
            break;
        }
    }

    return total;
};

export default MultipleChart;
