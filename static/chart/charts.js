import DailyChart from '../chart/daily-chart';
import CumulativeChart from '../chart/cumulative-chart';
import MultipleChart from '../chart/multiple-chart';
import TheatersChart from '../chart/theaters-chart';
import PerTheaterAverageChart from '../chart/per-theater-average-chart';
import WeeklyChangeChart from '../chart/weekly-change-chart';

export default {
    'daily': DailyChart,
    'cumulative': CumulativeChart,
    'multiple': MultipleChart,
    'theaters': TheatersChart,
    'per-theater': PerTheaterAverageChart,
    'weekly-change': WeeklyChangeChart
};
