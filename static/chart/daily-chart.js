import BaseChart from './base-chart'

function DailyChart() {
    BaseChart.apply(this, arguments);
}

DailyChart.prototype = Object.create(BaseChart.prototype);

DailyChart.prototype.getTitleText = function getTitleText() {
    return 'Daily box office';
};

DailyChart.prototype.getValue = function getValue(daily, index, boxOffice) {
    return daily.gross;
};

export default DailyChart;
