import BaseChart from './base-chart'

function CumulativeChart() {
    BaseChart.apply(this, arguments);
}

CumulativeChart.prototype = Object.create(BaseChart.prototype);

CumulativeChart.prototype.getTitleText = function getTitleText() {
    return 'Cumulative box office';
};

CumulativeChart.prototype.getValue = function getValue(daily, index, boxOffice) {
    return daily.cumulative;
};

export default CumulativeChart;
