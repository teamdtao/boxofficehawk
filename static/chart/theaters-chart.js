import BaseChart from './base-chart'

function TheatersChart() {
    BaseChart.apply(this, arguments);
}

TheatersChart.prototype = Object.create(BaseChart.prototype);

TheatersChart.prototype.getTitleText = function getTitleText() {
    return 'Theater count';
};

TheatersChart.prototype.getValue = function getValue(daily, index, boxOffice) {
    return daily.theaters;
};

export default TheatersChart;
