import Highcharts from 'highcharts';

function BaseChart(element, matchupData) {
    this.element = element;
    this.matchupData = matchupData;
}

BaseChart.prototype.render = function render(results, options) {
    this.processResults(results);
    Highcharts.chart(this.element, this._getChartOptions(results, options));
};

BaseChart.prototype.processResults = function processResults(results) {
};

BaseChart.prototype.getTitle = function getTitle() {
    var title = {
        text: this.getTitleText()
    };

    if (this.matchupData) {
        title.text += ': <a href="' + this.matchupData.url + '" target="_top">' + this.matchupData.title + '</a>';
        title.useHTML = true;
    }

    return title;
};

BaseChart.prototype.getTitleText = function getTitleText() {
    throw Error('not implemented');
};

BaseChart.prototype.getValue = function getValue(daily, index, boxOffice) {
    throw Error('not implemented');
};

BaseChart.prototype._getChartOptions = function _getChartOptions(results, options) {
    var self = this,
        start = options.start || 0,
        count = options.count;

    if (start || count) {
        results = results.map(function(result) {
            return {
                title: result.title,
                box_office: result.box_office.slice(start, start + count)
            };
        });
    }

    var xAxis = {
        title: {
            'text': 'Weeks in release'
        },
        labels: {
            formatter: function() {
                var week = Math.floor((start + this.value) / 7) + 1;
                return self._formatFriday(week);
            }
        },
        tickPositioner: function() {
            var positions = [];

            var largestResult = results.reduce(function(a, b) {
                return a.box_office.length > b.box_office.length ? a : b;
            }, results[0]);

            largestResult.box_office.forEach(function(daily, i) {
                if (daily.day.slice(0, 3) == 'Fri') {
                    positions.push(i + 1);
                }
            });

            // Don't display *too* many data labels (e.g. when using
            // "Forever" as the time period).
            if (positions.length > 10) {
                positions = positions.filter(function(pos, i) {
                    return i % 2 == 0;
                });
            }

            return positions;
        }
    };

    var yAxis = {
        title: {
            text: this.getTitleText()
        }
    };

    if (options.prediction) {
        yAxis.plotLines = [{
            color: '#f00',
            dashStyle: 'Dash',
            width: 2,
            value: options.prediction
        }];
    }

    return {
        chart: {
            zoomType: 'x'
        },
        title: this.getTitle(),
        xAxis: xAxis,
        yAxis: yAxis,
        plotOptions: {
            line: {
                marker: {
                    enabled: false
                }
            },
            series: {
                pointStart: 1
            }
        },
        legend: {
            labelFormatter: function() {
                return this.name + ' <a href="' + results[this.index].href +
                    '" target="_blank"><small><span class="glyphicon ' +
                    'glyphicon-new-window"></span></small></a>';
            },
            useHTML: true
        },
        series: results.map(function(result, resultIndex) {
            var boxOffice = result.box_office;

            return {
                type: 'line',
                name: result.title,
                data: boxOffice.map(function(daily, index) {
                    return [daily.day, self.getValue(daily, index, boxOffice, resultIndex)];
                })
            };
        }),
        credits: {
            enabled: false
        }
    };
};

BaseChart.prototype._formatFriday = function _formatFriday(week) {
    var suffix;
    switch (week % 10) {
        case 1:
            suffix = 'st';
            break;
        case 2:
            if (week % 100 != 12) {
                suffix = 'nd';
            }
            break;
        case 3:
            if (week % 100 != 13) {
                suffix = 'rd';
            }
            break;
    }

    if (!suffix) {
        suffix = 'th';
    }

    return week + suffix + ' Friday';
};

export default BaseChart;
