import BaseChart from './base-chart'
import utils from '../js/utils';

function WeeklyChangeChart() {
    BaseChart.apply(this, arguments);
}

WeeklyChangeChart.prototype = Object.create(BaseChart.prototype);

WeeklyChangeChart.prototype.getTitleText = function getTitleText() {
    return 'Weekly % change';
};

WeeklyChangeChart.prototype.getValue = function getValue(daily, index, boxOffice) {
    if (index < 7) {
        return null;
    }

    return Math.round(daily.gross / boxOffice[index - 7].gross * 100);
};

WeeklyChangeChart.prototype._getChartOptions = function _getChartOptions(results, maxResults) {
    var options = BaseChart.prototype._getChartOptions.call(this, results, maxResults);
    return utils.merge(options, { xAxis: { min: 8 } });
};

export default WeeklyChangeChart;
