import BaseChart from './base-chart'

function PerTheaterAverageChart() {
    BaseChart.apply(this, arguments);
}

PerTheaterAverageChart.prototype = Object.create(BaseChart.prototype);

PerTheaterAverageChart.prototype.getTitleText = function getTitleText() {
    return 'Per-theater average';
};

PerTheaterAverageChart.prototype.getValue = function getValue(daily, index, boxOffice) {
    return daily.gross / daily.theaters;
};

export default PerTheaterAverageChart;
